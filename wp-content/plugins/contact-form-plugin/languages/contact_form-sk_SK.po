msgid ""
msgstr ""
"Project-Id-Version: contact_form\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-02-20 14:07+0300\n"
"PO-Revision-Date: 2014-02-20 14:07+0300\n"
"Last-Translator: bestwebsoft.com <plugins@bestwebsoft.com>\n"
"Language-Team: Book Ing <book.ing@zoznam.sk>\n"
"Language: cs_CZ\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-SearchPath-0: .\n"

#: contact_form.php:33
#: contact_form.php:660
msgid "Contact Form Settings"
msgstr "Nastavenie kontaktného formulára"

#: contact_form.php:33
msgid "Contact Form"
msgstr "Kontaktný formulár"

#: contact_form.php:80
#: contact_form.php:978
#: contact_form.php:1008
msgid "Name:"
msgstr "Meno:"

#: contact_form.php:81
#: contact_form.php:979
#: contact_form.php:1009
msgid "Address:"
msgstr "Adresa::"

#: contact_form.php:82
#: contact_form.php:980
#: contact_form.php:1010
msgid "Email Address:"
msgstr "E-Mail:"

#: contact_form.php:83
#: contact_form.php:981
#: contact_form.php:1011
msgid "Phone number:"
msgstr ""

#: contact_form.php:84
#: contact_form.php:982
#: contact_form.php:1012
msgid "Subject:"
msgstr "Predmet:"

#: contact_form.php:85
#: contact_form.php:983
#: contact_form.php:1013
msgid "Message:"
msgstr "Správa:"

#: contact_form.php:86
#: contact_form.php:984
#: contact_form.php:1014
msgid "Attachment:"
msgstr "Príloha"

#: contact_form.php:87
msgid "Supported file types: HTML, TXT, CSS, GIF, PNG, JPEG, JPG, TIFF, BMP, AI, EPS, PS, RTF, PDF, DOC, DOCX, XLS, ZIP, RAR, WAV, MP3, PPT. Max file size: 2MB"
msgstr ""

#: contact_form.php:88
#: contact_form.php:986
#: contact_form.php:1016
msgid "Send me a copy"
msgstr "Zaslať mi kópiu"

#: contact_form.php:89
#: contact_form.php:987
#: contact_form.php:1017
msgid "Submit"
msgstr "Odoslať"

#: contact_form.php:90
msgid "Your name is required."
msgstr "Vaše meno je vyžadované."

#: contact_form.php:91
msgid "Address is required."
msgstr "Adresa je vyžadovaná."

#: contact_form.php:92
msgid "A valid email address is required."
msgstr "Platný E-mail je vyžadovaný."

#: contact_form.php:93
msgid "Phone number is required."
msgstr "Telefónne číslo je vyžadované."

#: contact_form.php:94
msgid "Subject is required."
msgstr "Predmet je vyžadovaný."

#: contact_form.php:95
msgid "Message text is required."
msgstr "Správa je vyžadovaná."

#: contact_form.php:96
msgid "File format is not valid."
msgstr "Nesprávny typ súboru."

#: contact_form.php:97
msgid "File upload error."
msgstr ""

#: contact_form.php:98
msgid "The file could not be uploaded."
msgstr ""

#: contact_form.php:99
msgid "This file is too large."
msgstr ""

#: contact_form.php:100
msgid "Please fill out the CAPTCHA."
msgstr "Prosím opíšte CAPTCHA text."

#: contact_form.php:101
msgid "Please make corrections below and try again."
msgstr "Prosím opravte chyby a skúste to opäť."

#: contact_form.php:103
msgid "Thank you for contacting us."
msgstr "Ďakujeme za správu."

#: contact_form.php:203
#, fuzzy
msgid "requires"
msgstr "Vyžadované polia"

#: contact_form.php:203
msgid "or higher, that is why it has been deactivated! Please upgrade WordPress and try again."
msgstr ""

#: contact_form.php:203
msgid "Back to the WordPress"
msgstr ""

#: contact_form.php:203
#, fuzzy
msgid "Plugins page"
msgstr "Pro Plugins"

#: contact_form.php:494
msgid "If the 'Redirect to page' option is selected then the URL field should be in the following format"
msgstr "Ak je vybratá možnosť \"Presmerovanie na stránku\" potom pole URL by malo mať nasledujúci formát"

#: contact_form.php:503
msgid "Such user does not exist. Settings are not saved."
msgstr "Taký užívateľ neexistuje. Nastavenie sa neuloží."

#: contact_form.php:507
#: contact_form.php:513
msgid "Please enter a valid email address in the 'FROM' field. Settings are not saved."
msgstr "Zadajte platnú e-mailovú adresu v poli \"Od\". Nastavenie sa neuloží."

#: contact_form.php:518
msgid "Settings saved."
msgstr "Nastavenie uložené."

#: contact_form.php:554
#: contact_form.php:593
msgid "Wrong license key"
msgstr ""

#: contact_form.php:586
msgid "Something went wrong. Try again later. If the error will appear again, please, contact us <a href=http://support.bestwebsoft.com>BestWebSoft</a>. We are sorry for inconvenience."
msgstr ""

#: contact_form.php:595
msgid "This license key is bind to another site"
msgstr ""

#: contact_form.php:597
#: contact_form.php:1392
msgid "Unfortunately, you have exceeded the number of available tries per day. Please, upload the plugin manually."
msgstr ""

#: contact_form.php:615
msgid "Failed to open the zip archive. Please, upload the plugin manually"
msgstr ""

#: contact_form.php:621
msgid "Your server does not support either ZipArchive or Phar. Please, upload the plugin manually"
msgstr ""

#: contact_form.php:625
#: contact_form.php:634
msgid "Failed to download the zip archive. Please, upload the plugin manually"
msgstr ""

#: contact_form.php:638
msgid "Something went wrong. Try again later or upload the plugin manually. We are sorry for inconvienience."
msgstr ""

#: contact_form.php:653
#, fuzzy
msgid "Please, enter Your license key"
msgstr "Prosím zadajte vašu e-mailovú adresu..."

#: contact_form.php:662
msgid "Notice:"
msgstr ""

#: contact_form.php:662
msgid "The plugin's settings have been changed. In order to save them please don't forget to click the 'Save Changes' button."
msgstr ""

#: contact_form.php:665
#: contact_form.php:1997
#: contact_form.php:2008
msgid "Settings"
msgstr "Nastavenia"

#: contact_form.php:666
msgid "Extra settings"
msgstr "Extra nastavenia"

#: contact_form.php:667
msgid "Go PRO"
msgstr ""

#: contact_form.php:673
#: contact_form.php:1351
msgid "If you would like to add the Contact Form to your website, just copy and paste this shortcode to your post or page or widget:"
msgstr "Ak by ste chceli pridať kontaktný formulár na svoje webové stránky, stačí skopírovať a vložiť shortcode k príspevku, stránke alebo widgetu:"

#: contact_form.php:673
#: contact_form.php:674
#: contact_form.php:1001
#: contact_form.php:1050
msgid "or"
msgstr "alebo"

#: contact_form.php:674
msgid "If have any problems with the standard shortcode [contact_form], you should use the shortcode"
msgstr "Ak máte nejaké problémy so štandardným shortcode [contact_form], mali by ste použiť shortcode"

#: contact_form.php:675
msgid "They work the same way."
msgstr "Fungujú rovnakým spôsobom."

#: contact_form.php:676
msgid "If you leave the fields empty, the messages will be sent to the email address specified during registration."
msgstr "Ak necháte pole prázdne, budú správy posielané na e-mailovú adresu zadanú pri registrácii."

#: contact_form.php:680
msgid "The user's email address:"
msgstr "Užívateľova e-mailová adresa:"

#: contact_form.php:684
msgid "Create a username"
msgstr "Vytvorte užívateľské meno"

#: contact_form.php:689
msgid "Enter a username of the person who should get the messages from the contact form."
msgstr "Zadajte užívateľské meno osoby, ktorá by mala dostať správy z kontaktného formulára."

#: contact_form.php:693
msgid "Use this email address:"
msgstr "Použíte tento email::"

#: contact_form.php:696
msgid "Enter the email address you want the messages forwarded to."
msgstr "Zadajte e-mailovú adresu, ktorú chcete do správy presmerovať."

#: contact_form.php:702
msgid "Add department selectbox to the contact form:"
msgstr ""

#: contact_form.php:710
#: contact_form.php:1246
msgid "If you upgrade to Pro version all your settings will be saved."
msgstr ""

#: contact_form.php:715
#: contact_form.php:852
#: contact_form.php:923
#: contact_form.php:1096
msgid "This functionality is available in the Pro version of the plugin. For more details, please follow the link"
msgstr ""

#: contact_form.php:716
#: contact_form.php:853
#: contact_form.php:924
#: contact_form.php:1097
msgid "Contact Form Pro"
msgstr "Kontaktný formulár Pro"

#: contact_form.php:722
msgid "Save emails to the database"
msgstr ""

#: contact_form.php:732
msgid "Using"
msgstr ""

#: contact_form.php:732
#: contact_form.php:901
#: contact_form.php:904
#: contact_form.php:908
msgid "powered by"
msgstr ""

#: contact_form.php:735
#: contact_form.php:739
msgid "Using Contact Form to DB powered by"
msgstr ""

#: contact_form.php:735
#, fuzzy
msgid "Activate Contact Form to DB"
msgstr "Kontaktný formulár Pro"

#: contact_form.php:739
#, fuzzy
msgid "Download Contact Form to DB"
msgstr "Kontaktný formulár Pro"

#: contact_form.php:744
msgid "Additional options"
msgstr "Ďalšie nastavenia"

#: contact_form.php:746
msgid "Show"
msgstr ""

#: contact_form.php:747
msgid "Hide"
msgstr ""

#: contact_form.php:751
msgid "What to use?"
msgstr "Čo používať?"

#: contact_form.php:754
msgid "Wp-mail"
msgstr "Wp-mail"

#: contact_form.php:754
msgid "You can use the wp_mail function for mailing"
msgstr "Môžete použiť wp_mail funkcie pre zasielanie"

#: contact_form.php:756
msgid "Mail"
msgstr "Mail"

#: contact_form.php:756
msgid "To send mail you can use the php mail function"
msgstr "Ak chcete odosielať poštu, môžete použiť e-mailoé PHP funkcie"

#: contact_form.php:760
#, fuzzy
msgid "The text in the 'From' field"
msgstr "Zmena textu pola \"od\""

#: contact_form.php:762
msgid "User name"
msgstr ""

#: contact_form.php:763
msgid "The name of the user who fills the form will be used in the field 'From'."
msgstr ""

#: contact_form.php:766
#, fuzzy
msgid "This text will be used in the 'FROM' field"
msgstr "Táto e-mailová adresa bude použitá v poli 'od'."

#: contact_form.php:770
#, fuzzy
msgid "The email address in the 'From' field"
msgstr "Zadajte e-mailovú adresu v poli \"od\""

#: contact_form.php:772
msgid "User email"
msgstr ""

#: contact_form.php:773
msgid "The email address of the user who fills the form will be used in the field 'From'."
msgstr ""

#: contact_form.php:776
msgid "This email address will be used in the 'From' field."
msgstr "Táto e-mailová adresa bude použitá v poli 'od'."

#: contact_form.php:780
#, fuzzy
msgid "Required symbol"
msgstr "Vyžadované polia"

#: contact_form.php:790
msgid "Fields"
msgstr ""

#: contact_form.php:791
msgid "Used"
msgstr ""

#: contact_form.php:792
#, fuzzy
msgid "Required"
msgstr "Vyžadované polia"

#: contact_form.php:793
msgid "Visible"
msgstr ""

#: contact_form.php:794
msgid "Disabled for editing"
msgstr ""

#: contact_form.php:795
msgid "Field's default value"
msgstr ""

#: contact_form.php:800
#: contact_form.php:1121
#: contact_form.php:1841
#: contact_form.php:1871
msgid "Name"
msgstr "Meno"

#: contact_form.php:808
#: contact_form.php:1126
#: contact_form.php:1845
#: contact_form.php:1873
msgid "Address"
msgstr "Adresa"

#: contact_form.php:816
msgid "Email Address"
msgstr "Emailová adresa"

#: contact_form.php:824
#, fuzzy
msgid "Phone number"
msgstr "Tel. číslo"

#: contact_form.php:832
#: contact_form.php:1141
#: contact_form.php:1855
#: contact_form.php:1877
msgid "Subject"
msgstr "Predmet"

#: contact_form.php:840
#: contact_form.php:1145
#: contact_form.php:1858
#: contact_form.php:1878
msgid "Message"
msgstr "Správa"

#: contact_form.php:858
msgid "Attachment block"
msgstr "Blok príloh"

#: contact_form.php:860
msgid "Users can attach the following file formats"
msgstr "Užívatelia môžu pripojiť nasledujúce formáty súborov"

#: contact_form.php:874
msgid "Add to the form"
msgstr ""

#: contact_form.php:879
#, fuzzy
msgid "Tips below the Attachment"
msgstr "Zobrazenie tipov pod blokom príloh"

#: contact_form.php:888
#, fuzzy
msgid "'Send me a copy' block"
msgstr "Zobraz 'Zaslať kópiu' blok"

#: contact_form.php:901
#: contact_form.php:904
#: contact_form.php:908
#: contact_form.php:1155
msgid "Captcha"
msgstr ""

#: contact_form.php:904
msgid "Activate captcha"
msgstr "Aktivovať captcha"

#: contact_form.php:908
msgid "Download captcha"
msgstr "Stiahnuť captcha"

#: contact_form.php:916
msgid "Agreement checkbox"
msgstr ""

#: contact_form.php:916
msgid "Required checkbox for submitting the form"
msgstr ""

#: contact_form.php:917
msgid "Optional checkbox"
msgstr ""

#: contact_form.php:917
msgid "Optional checkbox, the results of which will be displayed in email"
msgstr ""

#: contact_form.php:928
msgid "Delete an attachment file from the server after the email is sent"
msgstr ""

#: contact_form.php:934
msgid "Email in HTML format sending"
msgstr ""

#: contact_form.php:938
msgid "Display additional info in the email"
msgstr "Zobraziť ďalšie informácie v e-maile"

#: contact_form.php:943
#: contact_form.php:1808
#: contact_form.php:1810
msgid "Sent from (ip address)"
msgstr "Odoslané z (ip adresa)"

#: contact_form.php:943
#, fuzzy
msgid "Example: Sent from (IP address):\t127.0.0.1"
msgstr "Odoslané z (ip adresa)"

#: contact_form.php:944
#: contact_form.php:1814
#: contact_form.php:1816
msgid "Date/Time"
msgstr "Datum / Čas"

#: contact_form.php:944
msgid "Example: Date/Time:\tAugust 19, 2013 8:50 pm"
msgstr ""

#: contact_form.php:945
#: contact_form.php:1820
#: contact_form.php:1822
msgid "Sent from (referer)"
msgstr "Odoslané z"

#: contact_form.php:945
msgid "Example: Sent from (referer):\thttp://bestwebsoft.com/contacts/contact-us/"
msgstr ""

#: contact_form.php:946
#: contact_form.php:1826
#: contact_form.php:1828
msgid "Using (user agent)"
msgstr "Použitie (user agent)"

#: contact_form.php:946
msgid "Example: Using (user agent):\tMozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36"
msgstr ""

#: contact_form.php:950
msgid "Language settings for the field names in the form"
msgstr "Nastavenie jazyka pre názvy polí vo formulári"

#: contact_form.php:959
msgid "Add a language"
msgstr "Pridať jazyk"

#: contact_form.php:963
msgid "Change the names of the contact form fields and error messages"
msgstr "Zmena názvov polí kontaktného formulára a chybových hlásení"

#: contact_form.php:968
#: contact_form.php:1041
msgid "English"
msgstr ""

#: contact_form.php:976
#: contact_form.php:1006
msgid "click to expand/hide the list"
msgstr ""

#: contact_form.php:985
#: contact_form.php:1015
#, fuzzy
msgid "Tips below the Attachment block"
msgstr "Zobrazenie tipov pod blokom príloh"

#: contact_form.php:988
#: contact_form.php:1018
msgid "Error message for the Name field"
msgstr ""

#: contact_form.php:989
#: contact_form.php:1019
msgid "Error message for the Address field"
msgstr ""

#: contact_form.php:990
#: contact_form.php:1020
msgid "Error message for the Email field"
msgstr ""

#: contact_form.php:991
#: contact_form.php:1021
msgid "Error message for the Phone field"
msgstr ""

#: contact_form.php:992
#: contact_form.php:1022
msgid "Error message for the Subject field"
msgstr ""

#: contact_form.php:993
#: contact_form.php:1023
msgid "Error message for the Message field"
msgstr ""

#: contact_form.php:994
#: contact_form.php:1024
msgid "Error message about the file type for the Attachment field"
msgstr ""

#: contact_form.php:995
#: contact_form.php:1025
msgid "Error message while uploading a file for the Attachment field to the server"
msgstr ""

#: contact_form.php:996
#: contact_form.php:1026
msgid "Error message while moving the file for the Attachment field"
msgstr ""

#: contact_form.php:997
#: contact_form.php:1027
msgid "Error message when file size limit for the Attachment field is exceeded"
msgstr ""

#: contact_form.php:998
#: contact_form.php:1028
msgid "Error message for the Captcha field"
msgstr ""

#: contact_form.php:999
#: contact_form.php:1029
msgid "Error message for the whole form"
msgstr ""

#: contact_form.php:1001
#: contact_form.php:1031
#: contact_form.php:1050
#: contact_form.php:1056
msgid "Use shortcode"
msgstr "Použite shortcode "

#: contact_form.php:1001
#: contact_form.php:1031
#: contact_form.php:1050
#: contact_form.php:1056
msgid "for this language"
msgstr "pre tento jazyk"

#: contact_form.php:1038
msgid "Action after email is sent"
msgstr "Akcia po zaslaní mailu."

#: contact_form.php:1040
msgid "Display text"
msgstr "Zobraziť text"

#: contact_form.php:1049
#: contact_form.php:1055
msgid "Text"
msgstr "Text"

#: contact_form.php:1062
msgid "Redirect to the page"
msgstr "Presmerovať na stránku"

#: contact_form.php:1063
msgid "Url"
msgstr "Url"

#: contact_form.php:1067
msgid "The $_SERVER variable that is used to build a URL of the form"
msgstr ""

#: contact_form.php:1071
msgid "If you are not sure whether to change this setting or not, please do not do that."
msgstr ""

#: contact_form.php:1077
#: contact_form.php:1252
msgid "Save Changes"
msgstr "Uložiť zmeny"

#: contact_form.php:1082
msgid "If you enjoy our plugin, please give it 5 stars on WordPress"
msgstr ""

#: contact_form.php:1083
#, fuzzy
msgid "Rate the plugin"
msgstr "Voľné pluginy"

#: contact_form.php:1086
#, fuzzy
msgid "If there is something wrong about it, please contact us"
msgstr "Ak máte akékoľvek otázky kontaktujte nás prostredníctvom"

#: contact_form.php:1101
msgid "Errors output"
msgstr ""

#: contact_form.php:1104
msgid "Display error messages"
msgstr ""

#: contact_form.php:1105
msgid "Color of the input field errors."
msgstr ""

#: contact_form.php:1106
msgid "Display error messages & color of the input field errors"
msgstr ""

#: contact_form.php:1111
msgid "Add placeholder to the input blocks"
msgstr ""

#: contact_form.php:1117
msgid "Add tooltips"
msgstr "Pridať popisky"

#: contact_form.php:1131
msgid "Email address"
msgstr "Emailová adresa"

#: contact_form.php:1136
msgid "Phone Number"
msgstr "Tel. číslo"

#: contact_form.php:1150
msgid "Attachment"
msgstr "Príloha"

#: contact_form.php:1155
msgid "(powered by bestwebsoft.com)"
msgstr ""

#: contact_form.php:1160
msgid "Style options"
msgstr "Nastavenie štýlu"

#: contact_form.php:1163
msgid "Text color"
msgstr ""

#: contact_form.php:1166
#: contact_form.php:1171
#: contact_form.php:1181
#: contact_form.php:1186
#: contact_form.php:1191
#: contact_form.php:1196
#: contact_form.php:1206
#: contact_form.php:1211
#: contact_form.php:1217
#: contact_form.php:1228
#: contact_form.php:1233
#: contact_form.php:1238
msgid "Default"
msgstr ""

#: contact_form.php:1168
msgid "Label text color"
msgstr ""

#: contact_form.php:1173
msgid "Placeholder color"
msgstr ""

#: contact_form.php:1178
msgid "Errors color"
msgstr ""

#: contact_form.php:1183
msgid "Error text color"
msgstr ""

#: contact_form.php:1188
msgid "Background color of the input field errors"
msgstr ""

#: contact_form.php:1193
msgid "Border color of the input field errors"
msgstr ""

#: contact_form.php:1198
msgid "Placeholder color of the input field errors"
msgstr ""

#: contact_form.php:1203
msgid "Input fields"
msgstr "Vložiť polia"

#: contact_form.php:1208
msgid "Input fields background color"
msgstr ""

#: contact_form.php:1213
msgid "Text fields color"
msgstr ""

#: contact_form.php:1215
msgid "Border width in px, numbers only"
msgstr ""

#: contact_form.php:1219
#: contact_form.php:1240
msgid "Border color"
msgstr ""

#: contact_form.php:1224
msgid "Submit button"
msgstr "Odoslať"

#: contact_form.php:1226
msgid "Width in px, numbers only"
msgstr ""

#: contact_form.php:1230
msgid "Button color"
msgstr ""

#: contact_form.php:1235
msgid "Button text color"
msgstr ""

#: contact_form.php:1256
msgid "Contact Form Pro | Preview"
msgstr "Kontaktný formulár Pro | Zobrazenie"

#: contact_form.php:1259
msgid "Show with errors"
msgstr ""

#: contact_form.php:1267
#: contact_form.php:1269
msgid "Please enter your full name..."
msgstr ""

#: contact_form.php:1280
#: contact_form.php:1282
msgid "Please enter your address..."
msgstr ""

#: contact_form.php:1291
#: contact_form.php:1293
msgid "Please enter your email address..."
msgstr "Prosím zadajte vašu e-mailovú adresu..."

#: contact_form.php:1302
#: contact_form.php:1304
msgid "Please enter your phone number..."
msgstr ""

#: contact_form.php:1313
#: contact_form.php:1315
msgid "Please enter subject..."
msgstr ""

#: contact_form.php:1323
#: contact_form.php:1325
msgid "Please enter your message..."
msgstr ""

#: contact_form.php:1367
msgid "Congratulations! The PRO version of the plugin is successfully download and activated."
msgstr ""

#: contact_form.php:1369
msgid "Please, go to"
msgstr ""

#: contact_form.php:1369
#, fuzzy
msgid "the setting page"
msgstr "Extra nastavenia"

#: contact_form.php:1370
msgid "You will be redirected automatically in 5 seconds."
msgstr ""

#: contact_form.php:1375
msgid "You can download and activate"
msgstr ""

#: contact_form.php:1377
msgid "version of this plugin by entering Your license key."
msgstr ""

#: contact_form.php:1379
msgid "You can find your license key on your personal page Client area, by clicking on the link"
msgstr ""

#: contact_form.php:1381
msgid "(your username is the email you specify when purchasing the product)."
msgstr ""

#: contact_form.php:1389
#: contact_form.php:1399
msgid "Go!"
msgstr ""

#: contact_form.php:1449
msgid "Sorry, email message could not be delivered."
msgstr "Prepáčte, správa nemohla byť doručená."

#: contact_form.php:1835
msgid "Contact from"
msgstr "Kontaktný formulár"

#: contact_form.php:1848
#: contact_form.php:1874
msgid "Email"
msgstr "E-Mail"

#: contact_form.php:1852
#: contact_form.php:1876
msgid "Phone"
msgstr "Telefón"

#: contact_form.php:1861
#: contact_form.php:1879
msgid "Site"
msgstr "Stránka"

#: contact_form.php:1941
msgid "If you can see this MIME, it means that the MIME type is not supported by your email client!"
msgstr "Ak vidíte túto MIME, znamená to, že MIME typ nie je podporovaný vyším e-mailovým klientom!"

#: contact_form.php:2009
msgid "FAQ"
msgstr "FAQ"

#: contact_form.php:2010
msgid "Support"
msgstr "Podpora"

#: contact_form.php:2068
msgid "Are you sure that you want to delete this language data?"
msgstr ""

#: contact_form.php:2223
msgid "It’s time to upgrade your <strong>Contact Form plugin</strong> to <strong>PRO</strong> version"
msgstr ""

#: contact_form.php:2224
msgid "Extend standard plugin functionality with new great options."
msgstr ""

#: contact_form.php:2233
msgid "<strong>Contact Form to DB</strong> allows to store your messages to the database."
msgstr ""

#: contact_form.php:2234
msgid "Manage messages that have been sent from your website."
msgstr ""

#~ msgid "Contact Form Pro Extra Settings"
#~ msgstr "Kontaktný formulár Pro Extra - nastavenie"

#~ msgid "Contact Form Pro | Extra Settings"
#~ msgstr "Kontaktný formulár | Extra nastavenie"

#~ msgid "Display fields"
#~ msgstr "Zobraz polia"

#~ msgid "Display tips below the Attachment block"
#~ msgstr "Zobrazenie tipov pod blokom príloh"

#~ msgid "Required fields"
#~ msgstr "Vyžadované polia"

#~ msgid "You can attach the following file formats"
#~ msgstr "Môžete pripojiť nasledujúce formáty súborov"

#~ msgid "Memory usage"
#~ msgstr "Využitie pamäte"

#~ msgid "Site URL"
#~ msgstr "Stránka URL"

#~ msgid "Please enter a valid email address."
#~ msgstr "Zadajte platnú emailovú adresu."

#~ msgid "Activated plugins"
#~ msgstr "Aktivované pluginy"

#~ msgid "Read more"
#~ msgstr "Čítať viac"

#~ msgid "Installed plugins"
#~ msgstr "Inštalované pluginy"

#~ msgid "Recommended plugins"
#~ msgstr "Doporučované pluginy"

#~ msgid "Download"
#~ msgstr "Stiahnuť"

#~ msgid "Install %s"
#~ msgstr "Inštalovaniee %s"

#~ msgid "Install now from wordpress.org"
#~ msgstr "Inštaluje teraz z wordpress.org"

#~ msgid "Active Plugins"
#~ msgstr "Aktivované pluginy"

#~ msgid "Inactive Plugins"
#~ msgstr "Neaktívne pluginy"

#~ msgid "Send to support"
#~ msgstr "Odoslať na podporu"

#~ msgid "Contact Form Options"
#~ msgstr "Nastavení kontaktního formuláře"

#~ msgid "Display Attachment tips"
#~ msgstr "Zobrazit vysvětlivky k příloze."

#~ msgid "Please enter a valid email address. Settings are not saved."
#~ msgstr "Vložte správný mail. Nastavení neuloženo."

#, fuzzy
#~ msgid "E-Mail Address"
#~ msgstr "E-Mail:"

#~ msgid "E-Mail Addresse:"
#~ msgstr "Indirizzo e-mail:"

#~ msgid "Install Now"
#~ msgstr "Installa Ora"
