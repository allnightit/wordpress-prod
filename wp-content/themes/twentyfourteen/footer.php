<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

		</div><!-- #main -->

		<footer id="colophon" class="site-footer" role="contentinfo">

			<?php get_sidebar( 'footer' ); ?>

			<div class="site-info">
				&copy; 2014 WEPA - All rights reserved
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div><!-- #page -->

	<?php wp_footer(); ?>
    
    

<!-- Liveagent Chat Code -->
<script type="text/javascript" id="la_x2s6df8d" src="https://chat.wepanow.com/liveagent/livechat/scripts/track.js"></script>
<img src="https://chat.wepanow.com/liveagent/livechat/scripts/pix.gif" onLoad="LiveAgentTracker.createButton('c8b8abc2', this);"/>
<!-- End Liveagent Chat Code -->

</body>
</html>