<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'abc123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`Q<*:SkKb`I)[HS%pE8vx,.PkS32_*u|!CGY1]?=hn#},mnjMzSsYtqCC,j2?Gx|');
define('SECURE_AUTH_KEY',  't(-{kDKeG06iLx+@tj||_ Q!^,=~-4,]_1P_frlr4eFf(pF^tv)J6@YLGa4#PqoA');
define('LOGGED_IN_KEY',    '-huAXbN`A5)YqEOS.}NTE+~q_+i6[%CpT} |eFr-Nv|}IwZiSbwVRk,|jPxI3 64');
define('NONCE_KEY',        'n1WKZ9E-]*O2W?uYs3$m)w1zCI^WXRiE{{+ Y@5<_Ln}3:Y}u;bB:QaH_B-:rv-{');
define('AUTH_SALT',        '?wM~znoW@a>$u3IX6VP+zyWeD/`w)J/>V+o@^a[]r`+A4x.,NS`7K(oxO*o)2[uV');
define('SECURE_AUTH_SALT', 'c5G{{|19=C]),[0&/BK_so-3B+:5_c3gF,o@NcZugn_%0H;UV`%F-8;Bo(Teo<Z)');
define('LOGGED_IN_SALT',   'iR}?.H?$|8{hCi q<}{+]uZ5jklJn9i9IcD+Q&puu`^tNo`m-!}^J0 SD3-H  +~');
define('NONCE_SALT',       'nA?I0WW!74K_[2}Zf+pFRwDi>9?zzirFN,-2>?-,]w$FWDQ]-aWI}[^O|Og+%!Bd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** Matt was here 2014 **/
